document.addEventListener('DOMContentLoaded', function(){ 
    
    document.getElementById('js-message').onkeyup = function () {
        document.getElementById('js-message-left-total').innerHTML =  this.value.length;
        document.getElementById('js-message-left-symbols').innerHTML =  200 - this.value.length;
    };

}, false);