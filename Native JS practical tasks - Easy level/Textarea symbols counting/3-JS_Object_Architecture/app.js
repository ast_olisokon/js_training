document.addEventListener('DOMContentLoaded', function(){ 
    /**
     * TextCounter
     */
    var TextCounter = {
        maxChar: 140,
        textArea: null,
        textAreaTotal: null,
        textAreaLeft: null,
        init: function() {
            this.textArea = document.querySelector('#js-message');
            this.textAreaTotal = document.querySelector('#js-message-left-total');
            this.textAreaLeft = document.querySelector('#js-message-left-symbols');
    
            this.events();
        },
    
        events: function() {},
        // ... put you methods here
    };

    TextCounter.init();
});