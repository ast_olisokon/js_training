document.addEventListener('DOMContentLoaded', function(){ 
    var message = document.getElementById('js-message');

    message.onkeyup = function (e) {
        var length = this.value.length;
        if(length > (140) ){
            document.getElementById('error').innerHTML = 'Max length is 140 characters only!';
            message.addEventListener('keypress', checkMessage, false);
        }
    };


    function checkMessage(evt) {
        evt = evt || window.event;
            var charCode = evt.which || evt.keyCode;
            var charStr = String.fromCharCode(charCode);
            if (/[\w-_./:;""''`*+=-?^${}()|[\]\\]/i.test(charStr)) {
                evt.preventDefault();
            }
        }

});